# Git Secure
#--
Git 安全存储库工具

## Command

|cmd|info|
|---|---|
|add|Add file contents to the index|
|clone|Clone a encrypted repository into a new directory|
|config|config your secure repository|
|commit|create a commit|
|diff|show commit changes between commit worktree,etc|
|init|initialize a secure repository|
|help|print help information|
|key|create a aes key|
|pull|Fetch from and integrate with another repository or a local branch|
|push|Update remote refs along with associated objects|
|remote|set remote for secure repositroy|
|status|Show the working tree status|




## 系统依赖

此项目基于 PowerShell 编写，因此，你需要安装 PowerShell 6.0，PowerShell 目前支持 Windows，Linux 和 macOS

你可以去 Github PowerShell 项目的 Release 页面下载 PowerShell：[PowerShell release](https://github.com/PowerShell/PowerShell/releases)

在 Windows 系统上，默认的 Powershell 需要 5.0,也就是说 Windows 7,8.1 需要升级 Powershell 5.0 (Windows 10 默认为 5.0) [Windows Management Framework 5.0](https://www.microsoft.com/en-us/download/details.aspx?id=50395)

## 使用

用户需要在码云上创建一个空仓库。

初始化一个空仓库

```
git-secure init repo
```
创建 AES 密钥：

```
git-secure key
```

添加文件到暂存区：

```
git-secure add
```

创建 commit：

```
git-secure commit -m "create a new commit"
```

推送代码：

```
git-secure push
```

拉取代码（暂时不支持 fetch）：

```
git-secure pull
```

配置：

```
git-secure config aes.key xxxxx
```


## 功能

git-secure 使用 AES 256 对每一个常规文件加密，这里的常规文件是不包括 commit，及 `.gitignore`，`.gitattributes`，`.gitmodules` 这些文件。

## 贡献

我们欢迎用户提交 PR，请注意，我们要求代码使用 PowerShell 格式化工具格式化，建议使用 Visual Studio Code (PowerShell 扩展) 修改代码。
