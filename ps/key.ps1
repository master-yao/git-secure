#!/usr/bin/env pwsh

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/AesProvider"
New-AesKey -KeySize 256
