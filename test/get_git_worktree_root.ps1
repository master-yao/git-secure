
param(
    [string]$Path
)

if($null -eq $Path){
    $Path=$PWD
}

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/Git"

Push-Location
Set-Location $Path
Get-CurrentGitDir 
Pop-Location