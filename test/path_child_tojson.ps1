param(
    [string]$Path,
    [string]$JsonPath
)

$Obj=@()

Get-ChildItem -Path $Path -Exclude ".git" -Recurse|ForEach-Object {
    if($_.Attributes -ne 'Directory'){
        $xobj=@{}
        $xobj["LastWriteTimeUtc"]=$_.LastWriteTimeUtc
        $xobj["Relative"]=Resolve-Path -Relative $_.FullName
        #$xobj["SHA1"]=Get-FileHash -Algorithm "SHA1" -Path $_.FullName
        $Obj+=$xobj
    }
}

ConvertTo-Json -InputObject $Obj |Out-File -Path $JsonPath