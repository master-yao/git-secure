
$array = @()

for ($i = 61; $i -lt 66; $i++) {
    $uri = "https://gitee.com/gists?page=$i"
    Write-Host "check uri: $uri"
    $body = Invoke-WebRequest -Uri $uri
    foreach ($h in $body.Links) {
        if ($h.href -match "/\w+/codes/\w+$" -and $null -eq $h.class) {
            $text = Split-Path -Leaf $h.href
            $obj = @{}
            $obj["id"] = $text
            $obj["obj"] = $h
            $array += $obj
        }
    }
}

$list = @()



foreach ($a in $array) {
    if ($list.Contains($a.id)) {
        Continue
    }
    $list += $a.id
    $title = $a.obj.outerHTML.Substring($a.obj.outerHTML.IndexOf(">") + 1)
    $title = ( -Split $title)[0]
    if ($title.Contains("spring") -or $title.Contains(".php") -or $title.Contains(".js") -or $title.Contains("REST") -or $title.Contains("html")) {
        "$($a.id), Web开发技术"  |Out-File -Append -Encoding utf8 -FilePath "gists.txt"
    }
    elseif ($title.Contains("数据库") -or $title.Contains("SQL") -or $title.Contains("sql")) {
        "$($a.id), 数据库开发"  |Out-File -Append -Encoding utf8 -FilePath "gists.txt"
    }
    else {
        "$($a.id), 编程语言基础"  |Out-File -Append -Encoding utf8 -FilePath "gists.txt"
    }
    
}